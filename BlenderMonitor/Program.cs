﻿//Written by Peter Boos 2018 for the Blender comunity to use

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BlenderMonitor
{
    class Program
    {


        static void Main(string[] args)
        {
            int LastFrame = Ask("Last frame of the job");
            int Timeout = Ask("In how many seccond should a render be finsihed \nNote be on the safe side with this, take some extra time \nReply in secconds");
            int IntervalCheck = Ask("how often to check for progress in minutes");
            string renderpath = AskString("Input the render output path");
            string rendertpye = "*" + AskString("extension of the rendered files type it like:  .png");

            string BlenderExePath = AskString("Full exe path to Blender ea: c:\\blender\\blender.exe");
            string BlenderBlendFile = AskString("Full path to the blendfile ea:  c:\\data\\blend1.blend");

            string LogTime = renderpath + @"\FrameTimeLog.txt";

            DirectoryInfo d;

            string lastfilename = "-1";

            while (Convert.ToInt32(lastfilename) != LastFrame)
            {
                d = new DirectoryInfo(renderpath);
                FileInfo lastfile;
                DateTime lasttime;
                //  FileInfo[] files = d.GetFiles( rendertpye).OrderBy(p => p.Name).ToArray();


                List<FileInfo> files = SortbyNumbersOnTheEnd(
                    d.GetFiles(rendertpye).OrderByDescending(o => string.IsNullOrEmpty(o.Name)).ThenBy(o =>
                     {
                         string preresult;
                         int result;
                         preresult = Regex.Replace(o.Name, @"[^\d]", "");
                         int.TryParse(preresult,out   result);
                         return result;
                    }).ToList()
                 );
                lastfile = files.Last();

                // none of these sortings algorythms worked despite their interesting
                //.OrderBy(t => t.Name, new NumeralAlphaCompare()).ToArray(); // https://www.codeproject.com/Tips/543502/Numeric-before-alpha-sort-in-LINQ
                //.ToArray(); 
                //.OrderBy(x => x.Name).ToArray();
                //    Array.Sort(files, new AlphanumComparatorFast());  // https://www.dotnetperls.com/alphanumeric-sorting


                if (File.Exists(LogTime)) File.Delete(LogTime);
                if (files.Count() > 0)
                {
                    using (StreamWriter sw = File.AppendText(LogTime))
                        foreach (var item in files)
                        {
                            string s = item.Name + "\t" + item.LastWriteTime;//   GetLastWriteTime;

                            {
                                sw.WriteLine(s);
                            }
                        }
                }

                if (files.Count() > 1)
                {
                    if (NumberFromEnd(  lastfilename) == LastFrame) { Console.WriteLine("Ready"); Environment.Exit(0); }
                    lasttime = lastfile.LastWriteTime;
                }
                else
                {
                    lasttime = DateTime.Now.AddSeconds(Convert.ToDouble(Timeout) * -1);
                }
                //
               if (lasttime.AddSeconds(Convert.ToDouble(Timeout)) < DateTime.Now)
                {
                    if (BlenderisRunning())
                    {
                        //Blender is not responsive
                        KillBlender();
                        System.Threading.Thread.Sleep(1000 * 10);
                        LaunchBlender(BlenderExePath, BlenderBlendFile, Convert.ToInt32(lastfilename) + 1, LastFrame);
                    }
                    else
                    {
                        //Blender might have quit or exited, or was never started.
                        LaunchBlender(BlenderExePath, BlenderBlendFile, Convert.ToInt32(lastfilename) + 1, LastFrame); ;
                    }
                }
                System.Threading.Thread.Sleep(IntervalCheck * 6000);
            }
            Console.ReadLine();
        }




        static int Ask(string s)
        {
            Console.WriteLine(s);
            return Convert.ToInt32(Console.ReadLine());
        }
        static string AskString(string s)
        {
            Console.WriteLine(s);
            return Console.ReadLine();
        }

        static bool BlenderisRunning()
        {
            bool exists = Process.GetProcessesByName("Blender").Any();
            return exists;
        }

        static void KillBlender()
        {
            foreach (Process Blender in Process.GetProcessesByName("Blender"))
            {
                Blender.Kill();
            }
        }

        static void LaunchBlender(string BlenderItself, string blendfile, int start, int end)
        {

            string arg = "-b " + blendfile + " s " + start.ToString() + " e " + end.ToString() + " -a";
            ProcessStartInfo info = new ProcessStartInfo(BlenderItself, arg);
            info.UseShellExecute = true;
            Process processChild = Process.Start(info); // separate window
        }


//non used sorting algo's.
        public class AlphanumComparatorFast : IComparer
        {
            public int Compare(object x, object y)
            {
                string s1 = x as string;
                if (s1 == null)
                {
                    return 0;
                }
                string s2 = y as string;
                if (s2 == null)
                {
                    return 0;
                }

                int len1 = s1.Length;
                int len2 = s2.Length;
                int marker1 = 0;
                int marker2 = 0;

                // Walk through two the strings with two markers.
                while (marker1 < len1 && marker2 < len2)
                {
                    char ch1 = s1[marker1];
                    char ch2 = s2[marker2];

                    // Some buffers we can build up characters in for each chunk.
                    char[] space1 = new char[len1];
                    int loc1 = 0;
                    char[] space2 = new char[len2];
                    int loc2 = 0;

                    // Walk through all following characters that are digits or
                    // characters in BOTH strings starting at the appropriate marker.
                    // Collect char arrays.
                    do
                    {
                        space1[loc1++] = ch1;
                        marker1++;

                        if (marker1 < len1)
                        {
                            ch1 = s1[marker1];
                        }
                        else
                        {
                            break;
                        }
                    } while (char.IsDigit(ch1) == char.IsDigit(space1[0]));

                    do
                    {
                        space2[loc2++] = ch2;
                        marker2++;

                        if (marker2 < len2)
                        {
                            ch2 = s2[marker2];
                        }
                        else
                        {
                            break;
                        }
                    } while (char.IsDigit(ch2) == char.IsDigit(space2[0]));

                    // If we have collected numbers, compare them numerically.
                    // Otherwise, if we have strings, compare them alphabetically.
                    string str1 = new string(space1);
                    string str2 = new string(space2);

                    int result;

                    if (char.IsDigit(space1[0]) && char.IsDigit(space2[0]))
                    {
                        int thisNumericChunk = int.Parse(str1);
                        int thatNumericChunk = int.Parse(str2);
                        result = thisNumericChunk.CompareTo(thatNumericChunk);
                    }
                    else
                    {
                        result = str1.CompareTo(str2);
                    }

                    if (result != 0)
                    {
                        return result;
                    }
                }
                return len1 - len2;
            }
        }
        public class NumeralAlphaCompare : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                int nIndexX = x.Replace(":", " ").IndexOf(" ");
                int nIndexY = y.Replace(":", " ").IndexOf(" ");
                bool bSpaceX = nIndexX > -1;
                bool bSpaceY = nIndexY > -1;

                long nX;
                long nY;
                if (bSpaceX && bSpaceY)
                {
                    if (long.TryParse(x.Substring(0, nIndexX).Replace(".", ""), out nX)
                        && long.TryParse(y.Substring(0, nIndexY).Replace(".", ""), out nY))
                    {
                        if (nX < nY)
                        {
                            return -1;
                        }
                        else if (nX > nY)
                        {
                            return 1;
                        }
                    }
                }
                else if (bSpaceX)
                {
                    if (long.TryParse(x.Substring(0, nIndexX).Replace(".", ""), out nX)
                        && long.TryParse(y, out nY))
                    {
                        if (nX < nY)
                        {
                            return -1;
                        }
                        else if (nX > nY)
                        {
                            return 1;
                        }
                    }
                }
                else if (bSpaceY)
                {
                    if (long.TryParse(x, out nX)
                        && long.TryParse(y.Substring(0, nIndexY).Replace(".", ""), out nY))
                    {
                        if (nX < nY)
                        {
                            return -1;
                        }
                        else if (nX > nY)
                        {
                            return 1;
                        }
                    }
                }
                else
                {
                    if (long.TryParse(x, out nX)
                        && long.TryParse(y, out nY))
                    {
                        if (nX < nY)
                        {
                            return -1;
                        }
                        else if (nX > nY)
                        {
                            return 1;
                        }
                    }
                }
                return x.CompareTo(y);
            }

        }

        //made my own algo that works usually with blender
        struct NumericfileInfo
        {
            public int filenr; public FileInfo fileinfo;
        }
        static public int NumberFromEnd(string s)
        {
           return Convert.ToInt32(Regex.Match(s, @"/\d+$/"));
        }
        static public List<FileInfo> SortbyNumbersOnTheEnd(List<FileInfo> files)
        {
            List<NumericfileInfo> filesnrs = new List<NumericfileInfo>();
            foreach (FileInfo file in files)
            {
                NumericfileInfo nf = new NumericfileInfo();



                //  nf.filenr = Convert.ToInt32(Regex.Replace(file.Name, "[^0-9]+", string.Empty));
                nf.filenr = NumberFromEnd(file.Name);
                nf.fileinfo = file;
                filesnrs.Add(nf);
            }
            filesnrs.Sort((s1, s2) => s1.filenr.CompareTo(s2.filenr));
            files.Clear();
            foreach (NumericfileInfo fnr in filesnrs)
            {
                files.Add(fnr.fileinfo);
            }
            return files;
        }



    }
}
